public class Triangle {

    Triangle(int a, int b, int c) {
        if (!negativeNumberCheck(a, b, c)) return;
        boolean result = checkSides(a, b, c);
    }

    public static boolean checkSides(int a, int b, int c) {
        if (!(a + b > c && c + a > b && c + b > a)) {
            System.out.println("Треугольник не получился | |=_=|");
            return false;
        }
        System.out.println("Треугольник получился ! | (o_o)");
        return true;
    }

    private boolean negativeNumberCheck(int a, int b, int c) {
        int[] arr = {a, b, c};
        for (int i : arr) {
            if (i < 1) {
                System.out.println("Число не может быть меньше 1 | |-_-|");
                return false;
            }
        }
        return true;
    }
}
